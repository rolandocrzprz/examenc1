/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenc1;

/**
 *
 * @author Rolando Cruz
 */
public class Docente extends Empleado {

    private int nivel;
    private float hrlaboradas;
    private float pagoHora;

    public Docente() {
    }

    public Docente(int nivel, float hrlaboradas, float pagoHora, int numEmpleado, String nombre, String domicilio, Contrato cont) {
        super(numEmpleado, nombre, domicilio, cont);
        this.nivel = nivel;
        this.hrlaboradas = hrlaboradas;
        this.pagoHora = pagoHora;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getHrlaboradas() {
        return hrlaboradas;
    }

    public void setHrlaboradas(float hrlaboradas) {
        this.hrlaboradas = hrlaboradas;
    }

    public float getPagoHora() {
        return pagoHora;
    }

    public void setPagoHora(float pagoHora) {
        this.pagoHora = pagoHora;
    }

    @Override
    public float calcularTotal() {
        float total = 0.0f;
        switch (this.nivel) {
            case 1:
                total = this.hrlaboradas * this.pagoHora * 0.35f;
                break;
            case 2:
                total = this.hrlaboradas * this.pagoHora * 0.4f;
                break;
            case 3:
                total = this.hrlaboradas * this.pagoHora * 0.5f;
                break;
        }
        return total;

    }

    @Override
    public float calcularImpuesto() {
        return this.hrlaboradas * this.pagoHora * this.cont.getImpuestoIsr() / 100;

    }

}
