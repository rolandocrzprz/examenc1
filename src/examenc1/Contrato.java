/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenc1;

/**
 *
 * @author Rolando Cruz
 */
public class Contrato {

    private String claveContrato;
    private int puesto;
    private float impuestoIsr;

    public Contrato() {
        this.claveContrato = "";
        this.puesto = 0;
        this.impuestoIsr = 0.0f;
    }

    public Contrato(String claveContrato, int puesto, float impuestoIsr) {
        this.claveContrato = claveContrato;
        this.puesto = puesto;
        this.impuestoIsr = impuestoIsr;
    }

    
    public String getClaveContrato() {
        return claveContrato;
    }

    public void setClaveContrato(String claveContrato) {
        this.claveContrato = claveContrato;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public float getImpuestoIsr() {
        return impuestoIsr;
    }

    public void setImpuestoIsr(float impuestoIsr) {
        this.impuestoIsr = impuestoIsr;
    }

}
