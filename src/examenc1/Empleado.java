/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenc1;

/**
 *
 * @author Rolando Cruz
 */
public abstract class Empleado {

    protected int numEmpleado;
    protected String nombre;
    protected String domicilio;
    protected Contrato cont;

    public Empleado() {
        this.numEmpleado = 0;
        this.nombre = "";
        this.domicilio = "";
        this.cont = new Contrato();
    }

    public Empleado(int numEmpleado, String nombre, String domicilio, Contrato cont) {
        this.numEmpleado = numEmpleado;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.cont = cont;
    }

    public int getNumEmpleado() {
        return numEmpleado;
    }

    public void setNumEmpleado(int numEmpleado) {
        this.numEmpleado = numEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public Contrato getCont() {
        return cont;
    }

    public void setCont(Contrato cont) {
        this.cont = cont;
    }

    public abstract float calcularTotal();

    public abstract float calcularImpuesto();
}
